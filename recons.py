import h5py
import numpy as np
from matplotlib import rc
rc('font',**{'size':'10','family':'sans-serif','sans-serif':['Computer Modern Sans serif']})
rc('text.latex', preamble='\\usepackage{amsmath},\\usepackage{amssymb}')
rc('text', usetex=True)
rc('font', size=10)
from scipy.optimize import fmin
from scipy.ndimage.filters import *
import matplotlib.pyplot as plt
from scipy import interpolate

from os.path import expanduser
home = expanduser("~")
if home == '/home/sma74':
    home = '/local/scratch/public/sma74-dropbox'
